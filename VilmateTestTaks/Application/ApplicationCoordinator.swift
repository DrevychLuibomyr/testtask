//
//  ApplicationCoordinator.swift
//  VilmateTestTaks
//
//  Created by Drevych Liubomyr on 6/27/19.
//  Copyright © 2019 Drevych Liubomyr. All rights reserved.
//

import Foundation
import UIKit

final class ApplicationCoordinator: Coordinator {
    
    //MARK: - Properties
    let window: UIWindow
    
    //MARK: - Constructor
    init(with window: UIWindow, dependencies: DependencyContainer, router: RouterType) {
        self.window = window
        super.init(router: router, dependencies: dependencies)
    }
    
    //MARK: - Method
    func start() {
        window.rootViewController = router.navigationController
        window.makeKeyAndVisible()
        let input = EventListInput(with: self)
        let vc = dependencies.makeCalendarEventListViewController(with: input)
        router.push(vc, animated: true)
    }
    
}
