//
//  Coordinator.swift
//  VilmateTestTaks
//
//  Created by Drevych Liubomyr on 6/27/19.
//  Copyright © 2019 Drevych Liubomyr. All rights reserved.
//

import Foundation

protocol CoordinatorType {
    var router: RouterType { get }
    var dependencies: DependencyContainer { get }
}

open class Coordinator: NSObject, CoordinatorType {
    
    //MARK: - Properties
    public var childCoordinators: [Coordinator] = []
    
    var router: RouterType
    var dependencies: DependencyContainer
    
    //MARK: - Constructor
    init(router: RouterType, dependencies: DependencyContainer) {
        self.router = router
        self.dependencies = dependencies
        super.init()
    }
    
    //MARK: - Public Methods
    public func addChild(_ coordinator: Coordinator) {
        childCoordinators.append(coordinator)
    }
    
    public func removeChild(_ coordinator: Coordinator?) {
        if let coordinator = coordinator, let index = childCoordinators.firstIndex(of: coordinator) {
            childCoordinators.remove(at: index)
        }
    }
}
