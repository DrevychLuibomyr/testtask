//
//  DependencyFactory.swift
//  VilmateTestTaks
//
//  Created by Drevych Liubomyr on 6/27/19.
//  Copyright © 2019 Drevych Liubomyr. All rights reserved.
//

import Foundation

protocol DependecyFactory {
    func makeLocalNotificationService() -> LocalNotificationServiceProtocol
    func makeCalendarService() -> CalendarServiceType
}
