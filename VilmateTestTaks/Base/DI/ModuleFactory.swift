//
//  ModuleFactory.swift
//  VilmateTestTaks
//
//  Created by Drevych Liubomyr on 6/27/19.
//  Copyright © 2019 Drevych Liubomyr. All rights reserved.
//

import Foundation

protocol ModuleFactory {
    func makeCalendarEventListViewController(with input: EventListInput) -> CalendarEventsListViewController
    func makeEventDetailViewController(with input: EventDetailModuleInput) -> EventDetailViewController
}

