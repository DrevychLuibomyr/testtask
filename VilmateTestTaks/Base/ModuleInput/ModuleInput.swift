//
//  ModuleInput.swift
//  VilmateTestTaks
//
//  Created by Drevych Liubomyr on 6/28/19.
//  Copyright © 2019 Drevych Liubomyr. All rights reserved.
//

import Foundation

protocol ModuleInput {
    var parentCoordinator: CoordinatorType { get }
}
