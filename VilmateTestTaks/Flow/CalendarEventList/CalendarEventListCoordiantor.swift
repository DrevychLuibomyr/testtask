//
//  CalendarEventListCoordiantor.swift
//  VilmateTestTaks
//
//  Created by Drevych Liubomyr on 6/27/19.
//  Copyright © 2019 Drevych Liubomyr. All rights reserved.
//

import Foundation

protocol CalendarEventListCoordinatorType {
     func showDetail(with event: CalendarEvent)
}

final class CalendarEventListCoordiantor: CoordinatorType {
    
    //MARK: Properties
    var router: RouterType
    var dependencies: DependencyContainer
    
    //MARK: - Constructor
    init(with router: RouterType, dependencies: DependencyContainer) {
        self.router = router
        self.dependencies = dependencies
    }
}

//MARK: - CalendarEventListCoordinatorType
extension CalendarEventListCoordiantor: CalendarEventListCoordinatorType {
    func showDetail(with event: CalendarEvent) {
        let input = EventDetailModuleInput(with: self,
                                           eventId: event.id)
        let vc = dependencies.makeEventDetailViewController(with: input)
        router.push(vc, animated: true)
    }
}
