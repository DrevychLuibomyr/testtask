//
//  EventListInput.swift
//  VilmateTestTaks
//
//  Created by Drevych Liubomyr on 6/28/19.
//  Copyright © 2019 Drevych Liubomyr. All rights reserved.
//

import Foundation

struct EventListInput: ModuleInput {
    
    //MARK: - Properties
    let parentCoordinator: CoordinatorType
    
    //MARK: - Constructor
    init(with parent: CoordinatorType) {
        parentCoordinator = parent
    }
}
