//
//  EventTableViewCell.swift
//  VilmateTestTaks
//
//  Created by Drevych Liubomyr on 6/26/19.
//  Copyright © 2019 Drevych Liubomyr. All rights reserved.
//

import UIKit

final class EventTableViewCell: UITableViewCell {

    @IBOutlet private weak var eventTitle: UILabel!
    @IBOutlet private weak var notificationButton: UIButton!
    @IBOutlet private weak var attendeniesLabel: UILabel!
    
    //MARK: - Properties
    var tapHandler: ((CalendarEvent) -> Void)?
    var event: CalendarEvent?
    
    //MARK: - LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    //MARK: - Public Method
    func setup(with event: CalendarEvent) {
        self.event = event
        eventTitle.text = event.title
        notificationButton.isSelected = event.isNotificationSet
        if let attendencies = event.attendies {
            attendeniesLabel.text = "Attendencies: \((attendencies.map{ $0.email}).joined(separator: ","))"
        }else {
            attendeniesLabel.text = "No attendencies"
        }
    }
    
    //MARK: - Private
    @IBAction func setNotificaitonButtonAction(_ sender: UIButton) {
        guard var event = event else {
            return
        }
        sender.isSelected.toggle()
        event.shouldSendLocalPush = sender.isSelected
        event.isNotificationSet = sender.isSelected
        tapHandler?(event)
    }
    
}
