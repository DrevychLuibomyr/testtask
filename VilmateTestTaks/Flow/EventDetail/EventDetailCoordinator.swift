//
//  EventDetailCoordinator.swift
//  VilmateTestTaks
//
//  Created by Drevych Liubomyr on 6/28/19.
//  Copyright © 2019 Drevych Liubomyr. All rights reserved.
//

import Foundation

protocol EventDetailCoordinatorType { }

final class EventDetailCoordinator: CoordinatorType {
    
    //MARK: - Properties
    var router: RouterType
    var dependencies: DependencyContainer
    
    //MARK: - Constructor
    init(with router: RouterType, dependencies: DependencyContainer) {
        self.router = router
        self.dependencies = dependencies
    }
}

//MARK: - EventDetailCoordinatorType
extension EventDetailCoordinator: EventDetailCoordinatorType { }
