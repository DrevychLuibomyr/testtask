//
//  EventDetailModuleInput.swift
//  VilmateTestTaks
//
//  Created by Drevych Liubomyr on 6/28/19.
//  Copyright © 2019 Drevych Liubomyr. All rights reserved.
//

import Foundation

struct EventDetailModuleInput: ModuleInput {
    
    //MARK: - Properties
    var parentCoordinator: CoordinatorType
    var eventId: String
    
    //MARK: - Constructor
    init(with parentCoordinator: CoordinatorType, eventId: String) {
        self.parentCoordinator = parentCoordinator
        self.eventId = eventId
    }
}
