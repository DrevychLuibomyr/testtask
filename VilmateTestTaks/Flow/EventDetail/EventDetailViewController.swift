//
//  CalendarEventDetailViewController.swift
//  VilmateTestTaks
//
//  Created by Drevych Liubomyr on 6/28/19.
//  Copyright © 2019 Drevych Liubomyr. All rights reserved.
//

import UIKit

final class EventDetailViewController: UIViewController {
    
    //MARK: - Properties
    var eventId: String!
    var viewModel: EventDetailViewModelType!
    var coordinator: EventDetailCoordinatorType!
    
    //MARK: - Views
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    
    //MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        startFlow()
    }
    
    //MARK: - Public Method
    func startFlow() {
        guard let event = viewModel.getEvent(with: eventId) else {
            return
        }
        updateUI(for: event)
    }
    
    //MARK: - Private Method
    private func updateUI(for event: CalendarEvent) {
        titleLabel.text = event.title
        descriptionLabel.text = event.description ?? "No description"
    }

}
