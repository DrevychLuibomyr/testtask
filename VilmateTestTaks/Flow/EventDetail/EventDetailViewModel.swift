//
//  EventDetailViewModel.swift
//  VilmateTestTaks
//
//  Created by Drevych Liubomyr on 6/28/19.
//  Copyright © 2019 Drevych Liubomyr. All rights reserved.
//

import Foundation

protocol EventDetailViewModelType {
    func getEvent(with id: String) -> CalendarEvent?
}

final class EventDetailViewModel: ViewModelType {
    
    //MARK: - Properties
    let calendarService: CalendarServiceType
    
    //MARK: - Constructor
    init(with calendarService: CalendarServiceType) {
        self.calendarService = calendarService
    }
}

//MARK: - EventDetailViewModelType
extension EventDetailViewModel: EventDetailViewModelType {
    func getEvent(with id: String) -> CalendarEvent? {
        return calendarService.getEvent(with: id)
    }
}
