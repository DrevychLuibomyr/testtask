//
//  String+Extensions.swift
//  VilmateTestTaks
//
//  Created by Drevych Liubomyr on 6/27/19.
//  Copyright © 2019 Drevych Liubomyr. All rights reserved.
//

import Foundation

//MARK: - String+isGmail
extension String {
    func isGmail() -> Bool {
        return self.contains("gmail.com")
    }
}
